import React from 'react';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';

import AuthLoadingScreen from './screens/AuthLoading';
import TasksScreen from './screens/TasksScreen';

import IconButton from './components/IconButton';
import TaskFormScreen from './screens/TaskFormScreen';
import TaskCardScreen from './screens/TaskCardScreen';
import FilterScreen from './screens/FilterScreen';

const AppStack = createStackNavigator(
  {
    TasksScreen: {
      screen: TasksScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {},
          headerTitle: 'Список задач',
          headerRight: (
            <IconButton
              name="ios-switch"
              size={24}
              onPress={() => {
                navigation.navigate('FilterScreen');
              }}
            />
          ),
          headerLeft: (
            <IconButton
              name="ios-add"
              size={24}
              onPress={() => {
                navigation.navigate('TaskFormScreen');
              }}
            />
          )
        };
      }
    },
    TaskCardScreen: {
      screen: TaskCardScreen,
      navigationOptions: ({ navigation }) => {
        const id = (((navigation || {}).state || {}).params || {}).id;
        return {
          headerStyle: {},
          headerTitle: 'Задача',
          headerRight: (
            <IconButton
              name="ios-create"
              size={24}
              onPress={() => {
                navigation.navigate('TaskFormScreen', { id });
              }}
            />
          ),
          headerLeft: (
            <IconButton
              name="ios-arrow-back"
              size={24}
              onPress={() => {
                navigation.goBack();
              }}
            />
          )
        };
      }
    }
    // OrderDetail: {
    //   screen: OrderDetail
    // }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const RootStack = createStackNavigator(
  {
    Main: {
      screen: AppStack
    },
    TaskFormScreen: {
      screen: TaskFormScreen,
      navigationOptions: ({ navigation }) => {
        const id = (((navigation || {}).state || {}).params || {}).id;
        return {
          headerStyle: {},
          headerTitle: id ? 'Редактирование задачи' : 'Новая задача',
          headerRight: <View />,
          headerLeft: (
            <IconButton
              name="ios-close"
              size={24}
              onPress={() => {
                navigation.goBack();
              }}
            />
          )
        };
      }
    },
    FilterScreen: {
      screen: FilterScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {},
          headerTitle: 'Фильтр',
          headerRight: <View />,
          headerLeft: (
            <IconButton
              name="ios-close"
              size={24}
              onPress={() => {
                navigation.goBack();
              }}
            />
          )
        };
      }
    }
  },
  {
    mode: 'modal',
    navigationOptions: {
      header: null
    }
  }
);

const AuthLoadingStack = createStackNavigator(
  {
    AuthLoading: {
      screen: AuthLoadingScreen,
      navigationOptions: () => {
        return {
          header: null,
          headerStyle: {},
          headerTitle: null,
          headerRight: <View />,
          headerLeft: <View />
        };
      }
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const navigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingStack,
    App: RootStack
  },
  {
    initialRouteName: 'AuthLoading',
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
);

const App = createAppContainer(navigator);

export default App;
