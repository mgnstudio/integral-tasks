const TaskTypes = [
  {
    key: 'all',
    filterTitle: 'Все',
    itemTitle: ''
  },
  {
    key: 'simple',
    filterTitle: 'Обычные',
    itemTitle: 'Обычная'
  },
  {
    key: 'important',
    filterTitle: 'Важные',
    itemTitle: 'Важная'
  },
  {
    key: 'veryImportant',
    filterTitle: 'Очень важные',
    itemTitle: 'Очень важная'
  }
];

export { TaskTypes };
