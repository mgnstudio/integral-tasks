const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0;
    const v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

// Все для того что бы не подключать moment.js

const adjustForTimezone = dateStr => {
  const date = new Date(dateStr);
  let timeOffsetInMS = date.getTimezoneOffset() * 60000;
  date.setTime(date.getTime() - timeOffsetInMS);
  return date.toISOString();
};

const getDateFormat = _date => {
  const date = String(adjustForTimezone(_date)).split('T');
  const days = String(date[0]).split('-');
  const hours = String(date[1]).split(':');
  return `${days[2]}.${days[1]}.${days[0]} ${hours[0]}:${hours[1]}`;
};

export { uuidv4, getDateFormat };
