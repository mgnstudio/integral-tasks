import { seamlessImmutableReconciler } from 'redux-persist-seamless-immutable';
import { AsyncStorage } from 'react-native';

import { createStore, applyMiddleware, combineReducers } from 'redux';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import thunk from 'redux-thunk';

import tasksReducer from './tasks/reducer';

const dateTransform = createTransform(
  toDeshydrate =>
    JSON.stringify(toDeshydrate, (key, value) =>
      value instanceof Date ? value.toISOString() : value
    ),
  toRehydrate =>
    JSON.parse(toRehydrate, (key, value) =>
      typeof value === 'string' && value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/)
        ? new Date(value)
        : value
    )
);

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: seamlessImmutableReconciler,
  transforms: [dateTransform]
};

const rootRedusers = combineReducers({
  tasks: tasksReducer
});
const persistedReducer = persistReducer(persistConfig, rootRedusers);

const store = createStore(persistedReducer, applyMiddleware(thunk));
const persistor = persistStore(store);

export { store, persistor };
