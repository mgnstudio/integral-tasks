import * as types from './actionTypes';
import * as tasksSelectors from './reducer';
import { uuidv4 } from '../../libs/helper';

export const addTask = task => {
  return (dispatch, getState) => {
    const taskWithId = {
      ...task,
      id: uuidv4()
    };
    const items = tasksSelectors.getItems(getState()).concat(taskWithId);
    dispatch({ type: types.TASKS_CHANGELIST, items });
  };
};

export const updateTask = task => {
  return (dispatch, getState) => {
    const items = tasksSelectors.getItems(getState());
    const newItems = items.map(m => {
      return task.id === m.id ? task : m;
    });
    dispatch({ type: types.TASKS_CHANGELIST, items: newItems });
  };
};

export const removeTask = id => {
  return (dispatch, getState) => {
    const items = tasksSelectors.getItems(getState());
    const newItems = items.flatMap(m => {
      return id === m.id ? [] : m;
    });
    dispatch({ type: types.TASKS_CHANGELIST, items: newItems });
  };
};

export const doneTask = id => {
  return (dispatch, getState) => {
    const task = tasksSelectors.getItem(getState(), id);
    const finishedTask = {
      ...task,
      finish: new Date()
    };
    const items = tasksSelectors.getItems(getState());
    const newItems = items.map(m => {
      return finishedTask.id === m.id ? finishedTask : m;
    });
    dispatch({ type: types.TASKS_CHANGELIST, items: newItems });
  };
};
