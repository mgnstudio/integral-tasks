import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
  items: [],
  filter: 'all'
});

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.TASKS_CHANGELIST:
      return state.merge({
        items: action.items
      });
    case types.TASKS_SETFILTER:
      return state.merge({
        filter: action.filter
      });
    default:
      return state;
  }
}

export const getItems = state => {
  return state.tasks.items;
};

export const getFilteredItems = state => {
  return state.tasks.items.filter(f =>
    state.tasks.filter === 'all' ? true : f.importance === state.tasks.filter
  );
};

export const getFilterKey = state => {
  return state.tasks.filter;
};

export const getItem = (state, id) => {
  return state.tasks.items.find(f => f.id === id);
};
