import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as tasksSelectors from '../store/tasks/reducer';
import * as tasksActions from '../store/tasks/actions';
import TaskForm from '../components/TaskForm';
import Layout from '../components/Layout';

class TaskFormScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { item, dispatch, navigation } = this.props;
    return (
      <Layout>
        <TaskForm
          data={item}
          onSuccess={form => {
            if (form.id) {
              dispatch(tasksActions.updateTask(form));
            } else {
              dispatch(tasksActions.addTask(form));
            }
            navigation.goBack();
          }}
        />
      </Layout>
    );
  }
}

TaskFormScreen.propTypes = {
  item: PropTypes.instanceOf(Object),
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired
};
TaskFormScreen.defaultProps = {
  item: {
    id: null,
    title: '',
    description: '',
    importance: '',
    deadline: null,
    finish: null
  }
};

const mapStateToProps = (state, props) => {
  const id = ((((props || {}).navigation || {}).state || {}).params || {}).id;
  return {
    item: tasksSelectors.getItem(state, id)
  };
};

export default connect(mapStateToProps)(TaskFormScreen);
