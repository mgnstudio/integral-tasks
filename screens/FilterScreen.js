import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Layout from '../components/Layout';

import * as tasksSelectors from '../store/tasks/reducer';
import * as types from '../store/tasks/actionTypes';
import FilterList from '../components/FilterList';

class FilterScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { filter, navigation, dispatch } = this.props;
    return (
      <Layout>
        <FilterList
          selected={filter}
          onSet={value => {
            dispatch({
              type: types.TASKS_SETFILTER,
              filter: value
            });
            navigation.goBack();
          }}
        />
      </Layout>
    );
  }
}

FilterScreen.propTypes = {
  filter: PropTypes.string.isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    filter: tasksSelectors.getFilterKey(state)
  };
};

export default connect(mapStateToProps)(FilterScreen);
