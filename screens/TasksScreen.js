import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Layout from '../components/Layout';

import * as tasksSelectors from '../store/tasks/reducer';
import List from '../components/List';

class TasksScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { items } = this.props;
    return <Layout>{items && <List items={items} />}</Layout>;
  }
}

TasksScreen.propTypes = {
  items: PropTypes.arrayOf(Object).isRequired
};

const mapStateToProps = state => {
  return {
    items: tasksSelectors.getFilteredItems(state)
  };
};

export default connect(mapStateToProps)(TasksScreen);
