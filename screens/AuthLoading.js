import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import Layout from '../components/Layout';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this.runApp();
  }

  runApp() {
    const { navigation } = this.props;
    navigation.navigate('App');
  }

  render() {
    return (
      <Layout>
        <Text>Loading</Text>
      </Layout>
    );
  }
}

AuthLoadingScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  }).isRequired
};

export default AuthLoadingScreen;
