import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as tasksSelectors from '../store/tasks/reducer';
import * as tasksActions from '../store/tasks/actions';
import Card from '../components/Card';
import Layout from '../components/Layout';

class TaskCardScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { item, dispatch, navigation } = this.props;
    return (
      <Layout>
        {item && (
          <Card
            data={item}
            onRemove={() => {
              dispatch(tasksActions.removeTask(item.id));
              navigation.goBack();
            }}
            onDone={() => {
              dispatch(tasksActions.doneTask(item.id));
            }}
          />
        )}
      </Layout>
    );
  }
}

TaskCardScreen.propTypes = {
  item: PropTypes.instanceOf(Object).isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state, props) => {
  const id = ((((props || {}).navigation || {}).state || {}).params || {}).id;
  return {
    item: tasksSelectors.getItem(state, id)
  };
};

export default connect(mapStateToProps)(TaskCardScreen);
