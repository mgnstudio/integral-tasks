import React from 'react';
import { ScrollView } from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from './Button';
import { TaskTypes } from '../libs/dictionaries';
import { getDateFormat } from '../libs/helper';

const Wrapper = styled.View`
  padding: 0 15px;
  flex: 1;
`;

const Title = styled.Text`
  padding: 25px 0 5px;
  font-size: 26px;
`;
const Description = styled.Text`
  font-size: 14px;
  color: #ccc;
`;
const Importance = styled.Text`
  font-size: 16px;
  padding-bottom: 10px;
`;
const CompletedDate = styled.Text`
  font-size: 16px;
  padding-bottom: 10px;
`;

class Card extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { data, onRemove, onDone } = this.props;
    const importance = TaskTypes.find(f => f.key === data.importance) || {};
    return (
      <Wrapper>
        <ScrollView
          contentContainerStyle={{ paddingBottom: !data.finish ? 100 : 50 }}
          style={{ marginBottom: !data.finish ? -100 : -50 }}
        >
          <Title>{data.title}</Title>
          <Importance>{`Важность задачи: ${importance.itemTitle}`}</Importance>
          {data.finish && (
            <CompletedDate>{`Дата выполнения: ${getDateFormat(data.finish)}`}</CompletedDate>
          )}
          <Description>{data.description}</Description>
        </ScrollView>
        {!data.finish && <Button color="green" text="пометить как выполненную" onPress={onDone} />}
        <Button color="red" text="Удалить задачу" onPress={onRemove} />
      </Wrapper>
    );
  }
}

Card.propTypes = {
  data: PropTypes.instanceOf(Object).isRequired,
  onRemove: PropTypes.func.isRequired,
  onDone: PropTypes.func.isRequired
};

export default Card;
