import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';

import ListItem from './ListItem';

const Wrapper = styled.FlatList`
  flex: 1;
  margin: 0 15px;
`;
const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #ccc;
`;

class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { items, navigation } = this.props;
    return (
      <Wrapper
        showsVerticalScrollIndicator={false}
        initialNumToRender={10}
        maxToRenderPerBatch={5}
        data={items}
        keyExtractor={item => item.id}
        ItemSeparatorComponent={({}) => <Separator />}
        renderItem={({ item, index }) => {
          return (
            <ListItem
              key={index}
              item={item}
              onPress={() => {
                navigation.navigate('TaskCardScreen', {
                  id: item.id
                });
              }}
            />
          );
        }}
      />
    );
  }
}

List.propTypes = {
  items: PropTypes.arrayOf(Object).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  }).isRequired
};

export default withNavigation(List);
