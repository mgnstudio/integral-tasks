import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.TouchableOpacity`
  height: 40px;
  margin: 5px 0;
  background-color: ${props => props.color || '#fff'};
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`;

const Title = styled.Text`
  color: #fff;
`;

class Button extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { text, onPress, color } = this.props;
    return (
      <Wrapper onPress={onPress} color={color}>
        <Title>{text}</Title>
      </Wrapper>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  onPress: PropTypes.func.isRequired
};

Button.defaultProps = {
  color: '#fff'
};

export default Button;
