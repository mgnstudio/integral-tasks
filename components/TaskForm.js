import React from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Button from './Button';
import { TaskTypes } from '../libs/dictionaries';
import { getDateFormat } from '../libs/helper';

const FormWrapper = styled.View`
  padding: 0 17px;
  flex: 1;
`;

const FormScroll = styled.ScrollView`
  padding-top: 14px;
`;

const FormButton = styled(Button)`
  padding: 3px 0;
`;
const FormItem = styled.View`
  padding-bottom: 14px;
  border-bottom-width: ${props => props.borderBottom || 0};
  border-bottom-color: #ececec;
`;
const FormItemInput = styled.TextInput`
  border-bottom-width: 1px;
  border-bottom-color: #ececec;
  font-size: 16px;
  padding: 15px 0px;
`;
const ImportanceListItem = styled.TouchableOpacity`
  padding: 7px 0;
`;
const DateTimeInputWrapper = styled.TouchableOpacity``;

const ImportanceListItems = styled.View``;
const ImportanceListItemText = styled.Text`
  font-size: 14px;
  color: ${props => (props.selected ? 'red' : '#000')};
`;
const ItemTitle = styled.Text`
  font-size: 16;
  padding: 15px 0 7px 0;
  color: #ccc;
`;
const ItemTextValue = styled.Text`
  font-size: 16;
`;
const FormItemError = styled.Text`
  font-size: 10;
  color: red;
`;

class TaskForm extends React.Component {
  constructor(props) {
    super(props);

    const {
      data = {
        id: null,
        title: '',
        description: '',
        importance: '',
        deadline: null,
        finish: null
      }
    } = props;

    this.state = {
      id: data.id,
      title: {
        value: data.title,
        validation: null
      },
      description: {
        value: data.description,
        validation: null
      },
      importance: {
        value: data.importance,
        validation: null
      },
      deadline: data.deadline,
      finish: data.finish
    };
  }

  componentWillMount() {}

  showDeadlineDateTimePicker() {
    this.setState({ isDeadlineDateTimePickerVisible: true });
  }

  hideDeadlineDateTimePicker() {
    this.setState({ isDeadlineDateTimePickerVisible: false });
  }

  handleDeadlineDatePicked(date) {
    this.setState({
      deadline: date
    });
    this.hideDeadlineDateTimePicker();
  }

  showFinishDateTimePicker() {
    this.setState({ isFinishDateTimePickerVisible: true });
  }

  hideFinishDateTimePicker() {
    this.setState({ isFinishDateTimePickerVisible: false });
  }

  handleFinishDatePicked(date) {
    this.setState({
      finish: date
    });
    this.hideFinishDateTimePicker();
  }

  submit() {
    const { onSuccess } = this.props;
    const { id, title, description, importance, deadline, finish } = this.state;

    if (this.validate()) {
      const form = {
        id,
        title: title.value,
        description: description.value,
        importance: importance.value,
        deadline,
        finish
      };

      onSuccess(form);
    }
  }

  // Простая валидация
  validate() {
    const { title, description, importance } = this.state;
    let titleValidation = null;
    let descValidation = null;
    let importanceValidation = null;
    if (title.value.length === 0) {
      titleValidation = 'Укажите название';
    }
    if (description.value.length === 0) {
      descValidation = 'Укажите описание';
    }
    if (importance.value.length === 0) {
      importanceValidation = 'Укажите важность задачи';
    }

    if (titleValidation || descValidation || importanceValidation) {
      this.setState(prevState => {
        return {
          title: {
            ...prevState.title,
            validation: titleValidation
          },
          description: {
            ...prevState.description,
            validation: descValidation
          },
          importance: {
            ...prevState.importance,
            validation: importanceValidation
          }
        };
      });
      return false;
    }

    return true;
  }

  render() {
    const {
      id,
      importance,
      title,
      description,
      deadline,
      finish,
      isFinishDateTimePickerVisible,
      isDeadlineDateTimePickerVisible
    } = this.state;
    return (
      <FormWrapper>
        <FormScroll contentContainerStyle={{ paddingBottom: 50 }} style={{ marginBottom: -50 }}>
          <FormItem>
            <FormItemInput
              placeholder="Название"
              onChangeText={value => {
                this.setState({
                  title: {
                    value,
                    validation: null
                  }
                });
              }}
              value={title.value}
            />
            {title.validation && <FormItemError>{title.validation}</FormItemError>}
          </FormItem>
          <FormItem>
            <FormItemInput
              underlineColorAndroid="transparent"
              multiline
              returnKeyType="done"
              blurOnSubmit
              placeholder="Описание"
              onChangeText={value => {
                this.setState({ description: { value, validation: null } });
              }}
              value={description.value}
            />
            {description.validation && <FormItemError>{description.validation}</FormItemError>}
          </FormItem>
          <FormItem borderBottom="1px">
            <ItemTitle>Важность задачи</ItemTitle>
            <ImportanceListItems>
              {TaskTypes.filter(f => f.key !== 'all').map(m => {
                return (
                  <ImportanceListItem
                    key={m.key}
                    onPress={() => {
                      this.setState({
                        importance: {
                          value: m.key,
                          validation: null
                        }
                      });
                    }}
                  >
                    <ImportanceListItemText selected={m.key === importance.value}>
                      {m.itemTitle}
                    </ImportanceListItemText>
                  </ImportanceListItem>
                );
              })}
            </ImportanceListItems>
            {importance.validation && <FormItemError>{importance.validation}</FormItemError>}
          </FormItem>
          <FormItem borderBottom="1px">
            <DateTimeInputWrapper
              onPress={() => {
                this.showDeadlineDateTimePicker();
              }}
            >
              <ItemTitle>Когда задачу нужно выполнить</ItemTitle>
              <ItemTextValue>{deadline ? getDateFormat(deadline) : 'Не указана'}</ItemTextValue>
              <DateTimePicker
                mode="datetime"
                titleIOS="Укажите дату и время"
                confirmTextIOS="Подтвердить"
                cancelTextIOS="Отмена"
                locale="ru"
                minimumDate={new Date()}
                isVisible={isDeadlineDateTimePickerVisible}
                onConfirm={date => {
                  this.handleDeadlineDatePicked(date);
                }}
                onCancel={() => {
                  this.hideDeadlineDateTimePicker();
                }}
              />
            </DateTimeInputWrapper>
          </FormItem>
          <FormItem>
            <TouchableOpacity
              onPress={() => {
                this.showFinishDateTimePicker();
              }}
            >
              <ItemTitle>Когда задача была выполнена</ItemTitle>
              <ItemTextValue>{finish ? getDateFormat(finish) : 'Не указана'}</ItemTextValue>
            </TouchableOpacity>
            <DateTimePicker
              mode="datetime"
              titleIOS="Укажите дату и время"
              confirmTextIOS="Подтвердить"
              cancelTextIOS="Отмена"
              locale="ru"
              minimumDate={new Date()}
              isVisible={isFinishDateTimePickerVisible}
              onConfirm={date => {
                this.handleFinishDatePicked(date);
              }}
              onCancel={() => {
                this.hideFinishDateTimePicker();
              }}
            />
          </FormItem>
        </FormScroll>
        <FormButton
          text={id ? 'Обновить' : 'Добавить'}
          color="blue"
          onPress={() => {
            this.submit();
          }}
        />
      </FormWrapper>
    );
  }
}

TaskForm.propTypes = {
  data: PropTypes.instanceOf(Object),
  onSuccess: PropTypes.func.isRequired
};

TaskForm.defaultProps = {
  data: {
    id: null,
    title: '',
    description: '',
    importance: '',
    deadline: null,
    finish: null
  }
};

export default TaskForm;
