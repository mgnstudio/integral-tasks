import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { TaskTypes } from '../libs/dictionaries';
import { getDateFormat } from '../libs/helper';

const Wrapper = styled.TouchableOpacity`
  padding: 25px 0;
`;

const Title = styled.Text`
  font-size: 26px;
  padding-bottom: 5px;
`;
const Description = styled.Text`
  font-size: 14px;
  color: #ccc;
`;
const Importance = styled.Text`
  font-size: 16px;
  padding-bottom: 5px;
`;
const CompletedDate = styled.Text`
  font-size: 16px;
  padding-bottom: 10px;
`;
const Expired = styled.Text`
  font-size: 10px;
  color: red;
`;

class ListItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { item, onPress } = this.props;
    const importance = TaskTypes.find(f => f.key === item.importance) || {};
    return (
      <Wrapper onPress={onPress}>
        {item.deadline && item.finish > item.deadline && <Expired>Просрочена</Expired>}
        <Title>{item.title}</Title>
        <Importance>{`Важность задачи: ${importance.itemTitle}`}</Importance>
        {item.finish && (
          <CompletedDate>{`Дата выполнения: ${getDateFormat(item.finish)}`}</CompletedDate>
        )}
        <Description>{item.description}</Description>
      </Wrapper>
    );
  }
}

ListItem.propTypes = {
  item: PropTypes.instanceOf(Object).isRequired,
  onPress: PropTypes.func.isRequired
};

export default ListItem;
