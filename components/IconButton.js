import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';

class IconButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { onPress } = this.props;
    return (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          alignItems: 'center',
          justifyContent: 'center'
        }}
        onPress={onPress}
      >
        <Ionicons {...this.props} />
      </TouchableOpacity>
    );
  }
}

IconButton.propTypes = {
  onPress: PropTypes.func.isRequired
};

export default IconButton;
