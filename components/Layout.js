import React from 'react';
import { StatusBar } from 'react-native';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
`;

const Container = styled.View`
  flex: 1;
`;
class Layout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { children } = this.props;
    return (
      <Wrapper>
        <StatusBar barStyle="dark-content" />
        <Container>{children}</Container>
      </Wrapper>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
