import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { TaskTypes } from '../libs/dictionaries';

const Wrapper = styled.ScrollView`
  flex: 1;
  padding: 0 15px;
`;
const Item = styled.TouchableOpacity`
  height: 70px;
  display: flex;
  justify-content: center;
  border-bottom-width: ${props => (props.last ? 0 : 1)}px;
  border-bottom-color: #ececec;
`;
const ItemText = styled.Text`
  font-size: 16px;
  color: ${props => (props.selected ? 'red' : '#000')};
`;

class FilterList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    const { selected, onSet } = this.props;
    return (
      <Wrapper>
        {TaskTypes.map((m, i) => {
          return (
            <Item
              key={m.key}
              last={i === TaskTypes.length - 1}
              onPress={() => {
                onSet(m.key);
              }}
            >
              <ItemText selected={selected === m.key}>{m.filterTitle}</ItemText>
            </Item>
          );
        })}
      </Wrapper>
    );
  }
}

FilterList.propTypes = {
  selected: PropTypes.string.isRequired,
  onSet: PropTypes.func.isRequired
};

export default FilterList;
